import React, { useState, useEffect } from 'react';  
import Modal from 'react-modal';

function ModalDetail(props) {
    const movie = props.movie;
    const [modalIsOpen, setIsOpen] = useState(props.openModal);
    useEffect(() => {
        setIsOpen(props.openModal);
    }, [props.openModal]);

    const modalCustomStyles = {
        content : {
          top                   : '50%',
          left                  : '50%',
          right                 : 'auto',
          bottom                : 'auto',
          marginRight           : '-50%',
          transform             : 'translate(-50%, -50%)'
        }
    };

    const moviePoster = movie.poster_path
        ? <img src={`https://image.tmdb.org/t/p/w200/${movie.poster_path}`}  alt={movie.title} />
        : <p>No image available</p>;

    return (
        <Modal
            isOpen={modalIsOpen}
            onRequestClose={() =>setIsOpen(false)}
            style={modalCustomStyles}
            contentLabel="Example Modal"
        >
            <span style={{ float: 'right', color: 'red', cursor: 'pointer' }}onClick={() =>setIsOpen(false)}>X</span>
            {moviePoster}
            <h2>{movie.original_title}</h2>
            <p>Release date: {movie.release_date}</p>
            <p>{movie.overview}</p>
        </Modal>);
}


export default ModalDetail;