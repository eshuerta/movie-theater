import React, { useState } from 'react';  
import './MovieItem.css';
import ModalDetail from '../ModalDetail';

function MovieItem(props) {

    const [modalIsOpen, setIsOpen] = useState(false);

    const movie = props.data;
    const year = movie.release_date ? movie.release_date.split("-") : [""];
    const moviePoster = movie.poster_path
        ? <img src={`https://image.tmdb.org/t/p/w200/${movie.poster_path}`}  alt={movie.title} />
        : <p>No image available</p>;
    
    return  <>
                <div className={"movieItem"} key={movie.id}  onClick={() => setIsOpen(true)}>
                    {moviePoster}
                    <div className="movieInfo">
                        <span>{year[0]}</span>
                        <p>{movie.title}</p>              
                    </div>
                </div>
                { // DETAIL                 
                modalIsOpen &&  <ModalDetail movie={movie} openModal={modalIsOpen} />
                }   
      </>;
} 

  
/* 
// image url that want to check
let moviePoster = imageExist(moviePosterUrl)
    ? <img src={moviePosterUrl} onerror="" alt={movie.title} />
    : <p>No image available</p>;

const imageExist = (url) => {
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status !== 404;
}
*/
export default MovieItem;