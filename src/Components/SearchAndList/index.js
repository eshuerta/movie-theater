import React, {useState, useEffect } from 'react';  
import './SearchAndList.css';
import MovieItem from '../MovieItem';

import { searchMovies } from '../../actions/moviesActions';
import emptyStar from './emptyStar.png';
import fullStar from './fullStar.png';


function SearchAndList() { 
  const [movies, setMovies] = useState([]);
  const [moviesShown, setMoviesShown] = useState(null);
  const [rating, setRating] = useState(null);
 
  
  // OnLoad fetch recommended movies
  useEffect(() => {    
    searchMovies().then(data => {
      setMovies(data.results);
      setMoviesShown(data.results.length);
    });
  }, []);

  useEffect(() => {
    setMoviesShown((movies.filter(movie => movie.vote_average >= rating)).length);
  }, [rating, movies]);
  
  // SEARCH FUNCTIONS
  var timedSearch;
  const searchByName = searchValue => {
    // If the user typed at least 2 letters
    if (searchValue.length > 1) {
      // clear timeout as user press keys and start over the timer
      clearTimeout(timedSearch);
      // To avoid multiple calls while typing
      timedSearch = setTimeout(function() {
        searchMovies(searchValue).then(data => {
          setMovies(data.results);
        });
      }, 1000);
    } else {
      searchMovies().then(data => {
        setMovies(data.results);
      });
    }
  }

 


  return (
    <>
      <input type="text" className="inputSearch" placeholder="Search for a movie" onChange={(e) => searchByName(e.currentTarget.value)} />
      <div className="resultsFoundFilterContainer">
        <span className="resultsCount">
          {moviesShown > 0
            ? `Displaying ${moviesShown} results`
            : "No results found"}
        </span> 
        <span className="filter">
          - Filter by Rating:
          <img src={rating >= 2 ? fullStar : emptyStar} alt="0 - 2" onClick={() => rating === 2 ? setRating(0) : setRating(2)} />
          <img src={rating >= 4 ? fullStar : emptyStar} alt="3 - 4" onClick={() => rating === 4 ? setRating(0) : setRating(4)} />
          <img src={rating >= 6 ? fullStar : emptyStar} alt="5 - 6" onClick={() => rating === 6 ? setRating(0) : setRating(6)} />
          <img src={rating >= 8 ? fullStar : emptyStar} alt="7 - 8" onClick={() => rating === 8 ? setRating(0) : setRating(8)} />
          <img src={rating >= 10 ? fullStar : emptyStar} alt="9 - 10" onClick={() => rating === 10 ? setRating(0) : setRating(10)} />
        </span>
      </div>
      <div className="moviesList">
        {movies.map((movie, index) => { // Avoid those whose vote_average are below the user rating selected 
          return movie.vote_average >= rating && <MovieItem data={movie}  key={movie.id} /> 
        })}
      </div>
      
      
    </>
  );
} 
  
export default SearchAndList;