import axios from 'axios';
import {SERVER_URL, API_KEY} from '../Constants';



export const searchMovies = (searchValue) => {    
            
    let url;
    const generalParams = `api_key=${API_KEY}&language=en-US&page=1&include_adult=false`;

    // Empty search, get recommended movies
    if (!searchValue || searchValue === '') {
        url = `${SERVER_URL}/discover/movie?${generalParams}`;        
    } else if (searchValue.length > 1) {       
        url = `${SERVER_URL}/search/movie?query=${searchValue}&${generalParams}`;
    }

    return apiCall(url);
}


const apiCall = url => {

    return new Promise(resolve => {
        axios.get(url).then(function (response) {
            resolve(response.data || []);
        }).catch(function (error) {
            if (error.response) {
                // Request made and server responded
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                // The request was made but no response was received
                console.log(error.request);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
            }
        });
    });
}

