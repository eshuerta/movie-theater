import React from 'react';
import './App.css';
import SearchAndList from './Components/SearchAndList';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>The movie theater name</h1>
      </header>
      <SearchAndList />
    </div>
  );
}

export default App;
